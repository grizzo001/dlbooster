package it.uniba.di.lacam.ml.metalearners.boostdlfoil;


import java.util.ArrayList;

import org.semanticweb.owlapi.model.OWLIndividual;

/**
 * An abstract class for an inductive supervised model
 * Each example is represented as an integer
 * @author NF
 *
 */
public abstract class InductiveClassificationModel {
	
	protected LProblem problem;

	public InductiveClassificationModel(LProblem aProblem) {
		this.problem = aProblem;
	}
	
	public abstract void learn(ArrayList<Integer> posExs, ArrayList<Integer> negExs, ArrayList<Integer> undExs);

	public abstract int classify(OWLIndividual ind);

} // class
