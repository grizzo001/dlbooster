package it.uniba.di.lacam.ml.metalearners.boostdlfoil;



import java.util.ArrayList;

import org.semanticweb.owlapi.model.OWLClassExpression;

import it.uniba.di.lacam.ml.metalearners.boostdlfoil.LProblem;

/**
 * A class to implement  the notion of covered example (K \models C(a), where a is an individual)
 * @author Giuseppe
 *
 */
public class CoverFunction {

	/**
	 * @param prob
	 * @param concept
	 * @param covExs
	 * @return
	 */
	public static ArrayList<Integer> removeCovered(LProblem prob, OWLClassExpression concept, ArrayList<Integer> covExs) {
		
		ArrayList<Integer> unCovExs = new ArrayList<Integer>();
		
		for (Integer ex: covExs) {
			if (!prob.reasoner.isEntailed(prob.dataFactory.getOWLClassAssertionAxiom(concept,prob.allExamples[ex]))) 
				unCovExs.add(ex);			 
		}
		return unCovExs;
	}

	/**
	 * @param prob
	 * @param concept
	 * @param covExs
	 * @return
	 */
	public static ArrayList<Integer> covered(LProblem prob, OWLClassExpression concept, ArrayList<Integer> covExs) {
		
		ArrayList<Integer> newCovExs = new ArrayList<Integer>();
		
		for (Integer ex: covExs) {
			if (prob.reasoner.isEntailed(prob.dataFactory.getOWLClassAssertionAxiom(concept, prob.allExamples[ex]))) 
				newCovExs.add(ex);
		}
	
		
		return newCovExs;
	}

	public static ArrayList<Integer> weightingcovered(LProblem prob, OWLClassExpression newConcept,
			ArrayList<Integer> covPos) {
		// TODO Auto-generated method stub
		return null;
	}

}
