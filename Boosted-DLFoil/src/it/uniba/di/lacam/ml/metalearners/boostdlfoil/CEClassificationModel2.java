package it.uniba.di.lacam.ml.metalearners.boostdlfoil;


import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;

import org.semanticweb.owlapi.dlsyntax.renderer.DLSyntaxObjectRenderer;
import org.semanticweb.owlapi.io.OWLObjectRenderer;
import org.semanticweb.owlapi.model.OWLClassExpression;
import org.semanticweb.owlapi.model.OWLIndividual;

import it.uniba.di.lacam.ml.metalearners.boostdlfoil.InductiveClassificationModel;
import it.uniba.di.lacam.ml.metalearners.boostdlfoil.LProblem;

/**
 * An ensemble of models (concept, confidence)
 * @author Giuseppe
 *
 */
public class CEClassificationModel2 extends InductiveClassificationModel {
	static  int EnsembleSize;
	static final OWLObjectRenderer renderer = new DLSyntaxObjectRenderer();
	protected Collection<Triple<OWLClassExpression,Double,Double>> model;
	
	public CEClassificationModel2(LProblem aProblem) {
		super(aProblem);
		EnsembleSize=aProblem.nOfEnsemble;
	}

	public void learn(ArrayList<Integer> posExs, ArrayList<Integer> negExs, ArrayList<Integer> undExs) {
		//int score =-1;
		
		//do {
		model = BoostWDFL.induceConcept(problem, posExs, negExs, undExs); 
		
	
	};

	public  int classify(OWLIndividual ind) {
		double countPos=0;
		double countNeg=0;
		double countUnd=0;
		double threshold=1E-7;
		for (Triple<OWLClassExpression,Double, Double> m : model) {
			//threshold+=(Math.abs(m.getRight()-m.getRight2()));
		
		if (problem.reasoner.isEntailed(problem.dataFactory.getOWLClassAssertionAxiom(m.getLeft(),ind))) 
		{	countPos+=m.getRight();
			//System.out.println("positive classif."+renderer.render(m.getLeft())+"["+m.getRight()+"]");
		}
		else if (problem.reasoner.isEntailed(problem.dataFactory.getOWLClassAssertionAxiom(problem.dataFactory.getOWLObjectComplementOf(m.getLeft()),ind))) 
			{countNeg+=m.getRight();
			//System.out.println("neg classif. "+renderer.render(m.getLeft())+"["+m.getRight()+"]");
			}
		
		else
			countUnd+=m.getRight();
		}
		 //System.out.println("[classification: ]"+countPos+ "threshold "+threshold);
		if ((countUnd-countPos-countNeg)>0)
			return 0;
		if ((countPos-countNeg)>threshold)
			return +1;
		 else
			 if ((countPos-countNeg)<-threshold)
			 return -1;
			 else
				return 0;
	}

} // class
