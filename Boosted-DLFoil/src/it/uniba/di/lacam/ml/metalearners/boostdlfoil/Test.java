package it.uniba.di.lacam.ml.metalearners.boostdlfoil;

import org.semanticweb.owlapi.model.OWLClassExpression;

/**
 * Main class for running the program
 * @author Giuseppe
 *
 */
public class Test {

	public Test() {
		
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		LProblem problem = new LProblem(args[0]);

		//OWLClassExpression exp=problem.testConcepts[0];
		//RefinementOperator2.target=exp;
		
		if (problem.design.compareToIgnoreCase("cv")==0)
			Evaluation.crossValidation(problem); // run an experiment session
		else
			Evaluation.bootstrap(problem);
		
		System.out.print("\n\nEND: "+problem.urlOwlFile);
		System.out.printf("\t Seed: %d", problem.seed);
	}

}
