package it.uniba.di.lacam.ml.metalearners.boostdlfoil;

import java.util.ArrayList;
import java.util.HashMap;

import org.semanticweb.owlapi.model.OWLClassExpression;

/**
 * Utility class for updating the weights
 * @author Giuseppe
 *
 */
public class WeightsUtils {

	public static  HashMap<Integer, Double> posweights;
	public static  HashMap<Integer, Double> negweights;
	public static  HashMap<Integer, Double> undweights;
	public static void initializeWeights(HashMap<Integer, Double> p, HashMap<Integer, Double> n,HashMap<Integer, Double> u) {
		posweights=p;
		negweights=n;
		undweights=u;
	}
	public static double sum(HashMap<Integer, Double> posweights2) {
		// TODO Auto-generated method stub
		double sum=0.0;
		for(Integer i: posweights2.keySet()) {
			sum+= posweights2.get(i);
		}
		return sum;
	}


	public static double weights(ArrayList<Integer> newCovNeg,HashMap<Integer,Double> weights ) {
		double sum=0;		
		for (Integer i: newCovNeg) 
			sum=sum+weights.get(i);
		return sum;
	}


	public static ArrayList<Integer> omissions(ArrayList<Integer> covPos, ArrayList<Integer> newCovPos,
			ArrayList<Integer> newCovPosNeg) {
		ArrayList<Integer>newCovPosUnd= (ArrayList<Integer>)covPos.clone();
		newCovPosUnd.removeAll(newCovPos);
		newCovPosUnd.removeAll(newCovPosNeg);
		return newCovPosUnd;
	}


	
	static double weights(LProblem prob, OWLClassExpression newConcept, HashMap<Integer, Double> weights, int...membership) {
		double w=0.0d;

		ArrayList<Integer> coveredpos = CoverFunction.covered(prob, newConcept, new ArrayList<Integer>(weights.keySet()));
		ArrayList<Integer> covered= new ArrayList<>(weights.keySet());

		//System.out.println("[membership not null]");
		if (membership.length>0 &&membership[0]==0) {
			ArrayList<Integer> coveredNeg= CoverFunction.covered(prob, prob.dataFactory.getOWLObjectComplementOf(newConcept), new ArrayList<Integer>(weights.keySet()));
			coveredNeg.addAll(coveredpos);
			covered.removeAll(coveredNeg);
		}// the remaining individuals are correctly unlabeled
		else  covered= coveredpos;
		for (Integer p: covered)
			w+= weights.get(p);
		return w;
	}

	static double confidence(LProblem  prob, OWLClassExpression concept,HashMap<Integer,Double> posweights,HashMap<Integer,Double> negweights,HashMap<Integer,Double> undweights) {
		//double covPosweights=weights(prob, concept, posweights);
		//double covNegweights= weights(prob, prob.dataFactory.getOWLObjectComplementOf(concept), negweights);

		double corrwpos= weights(prob, concept, posweights);
		double corrwneg= weights(prob, prob.dataFactory.getOWLObjectComplementOf(concept), negweights);
		double corrund=  weights(prob, concept, undweights,0);


		// omission+ commission
		double wrongwpos= weights(prob, prob.dataFactory.getOWLObjectComplementOf(concept), posweights);
		double wrongwneg= weights(prob, concept, negweights);
		// induction
		double negwund= weights(prob, prob.dataFactory.getOWLObjectComplementOf(concept), undweights);
		double poswund= weights(prob, concept, undweights);
		double smoothFact= (double)1/(2*(posweights.size()+negweights.size()+undweights.size()));

		return Math.log((corrwpos+corrwneg+corrund+smoothFact)/(wrongwpos+wrongwneg+negwund+ poswund+smoothFact))/2;
	}

	private static double  h(LProblem prob,Integer ind,OWLClassExpression c) {
		double v=0.0d;
		boolean entailed = prob.reasoner.isEntailed(prob.dataFactory.getOWLClassAssertionAxiom(c,prob.allIndividuals[ind]));
		if (entailed) {
			v=1;
			//System.out.printf("[pos exs %d  covered]"+ v+"\n",ind);
		}
		else if (prob.reasoner.isEntailed(prob.dataFactory.getOWLClassAssertionAxiom(prob.dataFactory.getOWLObjectComplementOf(c),prob.allIndividuals[ind]))) {
			v= -1;//confidence(prob,c);
			//System.out.printf("[pos exs %d not covered  but covered by negation] \n"+ v, ind);
		}
		return v;
	}
	private static void updateProbabilities(LProblem prob,ArrayList<Integer> exs, OWLClassExpression c, HashMap<Integer, Double> weights, int label,OWLClassExpression c2) {
		//double y= label!=0?label:0;
		double confidence= confidence(prob,c,posweights,negweights,undweights);

		exs.forEach((e)->{
			//boolean entailed = prob.reasoner.isEntailed(prob.dataFactory.getOWLClassAssertionAxiom(c,prob.allIndividuals[e]))
				//	|| prob.reasoner.isEntailed(prob.dataFactory.getOWLClassAssertionAxiom(prob.dataFactory.getOWLObjectComplementOf(c),prob.allIndividuals[e]));
			double h = h(prob, e, c);
			double i= (label==h)? 1: 0;
			//if (label==-1) { //negative case
			// rule activated and updating weights
			//if (entailed) {
			double old= weights.get(e);
			double exp= -Math.pow(-1, i)* confidence;
			double num=old/Math.exp(exp); // decrementeranno soltanto sono corretti
			weights.replace(e, num);
			//System.out.println("ind:  l:"+label +" h:    "+h+"   "+e+"  "+old+" "+weights.get(e) );
			//}
			//}
		}
				);

		//		if (label==1) {
		//			exs.forEach((e)->{
		//				if (prob.reasoner.isEntailed(prob.dataFactory.getOWLClassAssertionAxiom(c, prob.allExamples[e]))) {
		//					double num=weights.get(e)/1;
		//					weights.replace(e, num);
		//				}
		//			});
		//			}
		//		if (label==0) {
		//			exs.forEach((e)->{
		//				if (prob.reasoner.isEntailed(prob.dataFactory.getOWLClassAssertionAxiom(c, prob.allExamples[e]))) {
		//					double num=weights.get(e)/Math.exp(confidence);
		//					weights.replace(e, num);
		//				}
		//			});
		//			}
		//		
		//		if (label==-1) {
		//			exs.forEach((e)->{
		//				if (prob.reasoner.isEntailed(prob.dataFactory.getOWLClassAssertionAxiom(c, prob.allExamples[e]))) {
		//					double num=weights.get(e)/Math.exp(confidence);
		//					weights.replace(e, num);
		//				}
		//			});
		//			}

	}

	public static void updateProbabilities(LProblem prob,ArrayList<Integer> posExs,ArrayList<Integer> negExs, ArrayList<Integer> undExs, OWLClassExpression c,HashMap<Integer,Double> posweights,HashMap<Integer,Double> negweights,HashMap<Integer,Double> undweights) {
		updateProbabilities(prob, posExs, c, posweights,+1, c);
		updateProbabilities(prob, negExs, c, negweights, -1,c);
		updateProbabilities (prob, undExs, c, undweights,0,c);

		double den=0;
		for (Integer i: posExs) {
			den+=posweights.get(i);
		}
		for (Integer i: negExs) {
			den+=negweights.get(i);
		}
		for (Integer i: undExs) {
			den+=undweights.get(i);
		}

		for (Integer i: posExs) {
			posweights.replace(i, posweights.get(i)/den);
			//System.out.println("[up pos weights"+i+"]"+posweights.get(i)/den);
		}
		for (Integer i: negExs) {
			negweights.replace(i, negweights.get(i)/den);
			//System.out.println("[up neg weights"+i+"]"+negweights.get(i)/den);
		}
		for (Integer i: undExs) {
			undweights.replace(i, undweights.get(i)/den);
			//System.out.println("[up und weights"+i+"]"+undweights.get(i)/den);
		}


	}
}
