package it.uniba.di.lacam.ml.metalearners.boostdlfoil;

import java.util.HashSet;
import java.util.Random;

import org.semanticweb.owlapi.model.ClassExpressionType;
import org.semanticweb.owlapi.model.OWLClass;
import org.semanticweb.owlapi.model.OWLClassExpression;
import org.semanticweb.owlapi.model.OWLObjectAllValuesFrom;
import org.semanticweb.owlapi.model.OWLObjectComplementOf;
import org.semanticweb.owlapi.model.OWLObjectProperty;
import org.semanticweb.owlapi.model.OWLObjectSomeValuesFrom;
import org.semanticweb.owlapi.model.OWLRestriction;

import it.uniba.di.lacam.ml.metalearners.boostdlfoil.LProblem;
import it.uniba.di.lacam.ml.metalearners.boostdlfoil.RandomGenerator;

/**
 * The refinement operator that specializes a concept adding a further conjunct or specializing an existing randomly selected sub description
 * Target language: ALC 
 * @author Giuseppe Rizzo
 *
 */
public class RefinementOperator {

	/**
	 * Specializes the current concept refining an existing one or adding a new conjunct(picked at random) 
	 * @param generator,  the random generator to make the choice of which concept constructor must be applied
	 * @param prob, the learning problem
	 * @param currConcept, the concept to be specialized
	 * @return a concept subsuming currConcept	 
	 */
	static OWLClassExpression rhoSingle(Random  generator, LProblem prob, OWLClassExpression currConcept) {
		
		OWLClassExpression refinement=null,	filler = null, refinedFiller=null;
		OWLObjectProperty property;
				
		if (generator.nextFloat() < 0.75)
			// refine concept
			switch (currConcept.getClassExpressionType()) {
			case OWL_CLASS: 
				OWLClass[] subCs = (OWLClass[]) prob.reasoner.getSubClasses(currConcept,false).entities().toArray(size -> new OWLClass[size]);
				refinement = (OWLClassExpression) subCs[RandomGenerator.generator.nextInt(subCs.length)];
				break;
			case OBJECT_COMPLEMENT_OF: 
				OWLClassExpression nestedConcept = ((OWLObjectComplementOf) currConcept).getOperand();
				OWLClass[] supCs = (OWLClass[]) prob.reasoner.getSuperClasses(nestedConcept,false).entities().toArray(size -> new OWLClass[size]);
				refinement = prob.dataFactory.getOWLObjectComplementOf(supCs[RandomGenerator.generator.nextInt(supCs.length)]); 
				break;
			case OBJECT_ALL_VALUES_FROM:
				property = (OWLObjectProperty) ((OWLRestriction) currConcept).getProperty();
				filler = ((OWLObjectAllValuesFrom) currConcept).getFiller();
				refinedFiller = rho(generator,prob,filler);
				refinement = prob.dataFactory.getOWLObjectAllValuesFrom(property, refinedFiller);
				break;
			case OBJECT_SOME_VALUES_FROM:
				property = (OWLObjectProperty) ((OWLRestriction) currConcept).getProperty();
				filler = ((OWLObjectSomeValuesFrom) currConcept).getFiller();
				refinedFiller = rho(generator,prob,filler);
				refinement = prob.dataFactory.getOWLObjectSomeValuesFrom(property, refinedFiller);
				break;
//			case DATA_ALL_VALUES_FROM:
//				break;
//			case DATA_EXACT_CARDINALITY:
//				break;
//			case DATA_HAS_VALUE:
//				break;
//			case DATA_MAX_CARDINALITY:
//				break;
//			case DATA_MIN_CARDINALITY:
//				break;
//			case DATA_SOME_VALUES_FROM:
//				break;
//			case OBJECT_EXACT_CARDINALITY:
//				break;
//			case OBJECT_HAS_SELF:
//				break;
//			case OBJECT_HAS_VALUE:
//				break;
//			case OBJECT_INTERSECTION_OF:
//				break;
//			case OBJECT_MAX_CARDINALITY:
//				break;
//			case OBJECT_MIN_CARDINALITY:
//				break;
//			case OBJECT_ONE_OF:
//				break;
//			case OBJECT_UNION_OF:
//				break;
			default:
				break;
			}
			else { // refine by adding conjunct
				OWLClassExpression newConcept = null;
				switch (RandomGenerator.generator.nextInt(4)) {
				case 0:
					newConcept = prob.allConcepts[RandomGenerator.generator.nextInt(prob.allConcepts.length)];
					break;
				case 1:
					newConcept = prob.allConcepts[RandomGenerator.generator.nextInt(prob.allConcepts.length)];
					newConcept = prob.dataFactory.getOWLObjectComplementOf(newConcept);
					break;
				case 2:
//					filler = prob.allConcepts[RandomGenerator.generator.nextInt(prob.allConcepts.length)];
					filler=prob.top;
					property = prob.allRoles[RandomGenerator.generator.nextInt(prob.allRoles.length)];
					newConcept = prob.dataFactory.getOWLObjectAllValuesFrom(property, filler);					
					break;
				case 3:
//					filler = prob.allConcepts[RandomGenerator.generator.nextInt(prob.allConcepts.length)];
					filler=prob.top;
					property = prob.allRoles[RandomGenerator.generator.nextInt(prob.allRoles.length)];
					newConcept = prob.dataFactory.getOWLObjectSomeValuesFrom(property, filler);					
					break;
				default:
					break;
				} // switch
				
				if (currConcept.equals(prob.top))
					refinement = newConcept;
				else {
					HashSet<OWLClassExpression> set = new HashSet<OWLClassExpression>();
					set.add(currConcept);
					set.add(newConcept); // was getRandomConcept
					refinement = prob.dataFactory.getOWLObjectIntersectionOf(set);
				}
			} // else add

		return refinement;

	}
	
	// refine conjunct of CEs 
	/**
	 * Refine a conjunction of Class Expressions (length >1)  and apply rhoSingle
	 * @param prob, the learning problem
	 * @param currConcept, the currentConcept
	 * @param i 
	 * @return
	 */
	static OWLClassExpression rhoCompound(Random generator, LProblem prob, OWLClassExpression currConcept, int i) {
		
		OWLClassExpression refinement=null;
		
								
		HashSet<OWLClassExpression> conjSet = (HashSet<OWLClassExpression>) currConcept.asConjunctSet();
		OWLClassExpression[] conjArray = conjSet.toArray(new OWLClassExpression[conjSet.size()]);	
		
		// select one of the CEs
		int selectedConjIndex = generator.nextInt(conjArray.length);
		OWLClassExpression conjunct2refine = conjArray[selectedConjIndex];
		// refine selected CE
		OWLClassExpression refinedConjunct = rho(generator,prob, conjunct2refine);
		// replace selected CE with the refinement 
		conjSet.remove(conjunct2refine);
		conjSet.add(refinedConjunct);

		refinement = prob.dataFactory.getOWLObjectIntersectionOf(conjSet);
			
		return refinement.getNNF();
	}
	
	
	
	/**
	 * Invoke one of the operators(rhoSingle and rhoCompund) while the specialized concept is not satisfiable
	 * @param prob
	 * @param currConcept, the concept to be speacialized
	 * @return  a new Class expression
	 */
	public static OWLClassExpression rho(Random generator, LProblem prob, OWLClassExpression currConcept) {
		
		OWLClassExpression refinement;

		do {
		
			if (currConcept.getClassExpressionType() == ClassExpressionType.OBJECT_INTERSECTION_OF)  
				// refine one of the conjuncts in the CE 
				refinement = rhoCompound(generator, prob, currConcept,0);
			else
				// replace with a refinement or add a new conjunct
				refinement = rhoSingle(generator,prob, currConcept);
		
//		} while (prob.reasoner.getInstances(refinement,false).entities().count()==0); // instead of deprecated method
		} while (!prob.reasoner.isSatisfiable(refinement));		
		
		return refinement;
	}

	
	
}
