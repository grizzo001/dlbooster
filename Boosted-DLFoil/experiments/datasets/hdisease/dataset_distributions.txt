---------------  GOLD STANDARD CLASSIFICATION ---------------

TARGET Concepts: 15
concept: 0	    pos:60     neg:44    und:535
concept: 1	    pos:34     neg:60    und:545
concept: 2	    pos:37     neg:60    und:542
concept: 3	    pos:60     neg:33    und:546
concept: 4	   pos:132     neg:60    und:447
concept: 5	    pos:60     neg:37    und:542
concept: 6	    pos:60     neg:60    und:519
concept: 7	    pos:34     neg:60    und:545
concept: 8	    pos:34     neg:60    und:545
concept: 9	    pos:44     neg:60    und:535
concept: 10	    pos:38     neg:60    und:541
concept: 11	    pos:60     neg:37    und:542
concept: 12	    pos:46     neg:60    und:533
concept: 13	    pos:60     neg:60    und:519
concept: 14	    pos:60     neg:60    und:519
