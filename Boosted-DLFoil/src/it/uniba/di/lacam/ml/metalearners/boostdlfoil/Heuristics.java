package it.uniba.di.lacam.ml.metalearners.boostdlfoil;


public class Heuristics {

	//
	/**
	 * the heuristic adopted by WDFL based on the difference of the weights between the groups of correctly and wrongly classified examples 
	 * in terms of the gain w.r.t. the concept to be specialized. N.B. The actual implementation considers only the difference for the refinement (it does not affect the results)   
	 * @param omatch, the sum of the weights of the correctly classified examples for the father concpet
	 * @param oerrors, the sum of the weights of the misclassified examples(commission/omission) for the father concpet 
	 * @param oind, the sum of the weights of misclassified examples (induction) for the father concpet
	 * @param match, the sum of the weights of the correctly classified examples for the new  specialization
	 * @param errors
	 * @param inductions
	 * @return
	 */
	public static double corr(double omatch, double oerrors, double oind, double match, double errors, double inductions) {
	
		//		double wig = ((double)nnPos/noPos)*(Math.log((nnPos+0.3)/(nnPos+nnNeg+1.0))-Math.log((noPos+0.3)/(noPos+noNeg+1.0)))/Math.log(2);
		double wig = //((double)(match)/(omatch))*
				(Math.sqrt(match)-Math.sqrt(errors+inductions));
	
		return wig;
	}


	
	/**
	 * the weighted information gain for dl-foil/focl
	 * @param noPos
	 * @param noNeg
	 * @param noUnd
	 * @param nnPos
	 * @param nnNeg
	 * @param nnUnd
	 * @return
	 */
	public static double wig(double noPos, double noNeg, double noUnd, double nnPos, double nnNeg, double nnUnd) {
	
		//		double wig = ((double)nnPos/noPos)*(Math.log((nnPos+0.3)/(nnPos+nnNeg+1.0))-Math.log((noPos+0.3)/(noPos+noNeg+1.0)))/Math.log(2);
		double wig = ((double)(nnPos)/(noPos))*
				(Math.log((nnPos+0.5)/(nnPos+nnNeg+nnUnd+1.0))-Math.log((noPos+0.5)/(noPos+noNeg+noUnd+1.0)))/Math.log(2);
	
		return wig;
	}

	
	
	
	/**
	 * Implementation of twoing criterion
	 * @param counts
	 * @return
	 */
	public static double twoing(int... counts) {	
	
		// twoing
		
		double totL = counts[Heuristics.PL]+counts[Heuristics.NL]+0.001;
		double totR = counts[Heuristics.PR]+counts[Heuristics.NR]+0.001;
		double tot = totL+totR;
		double pPL = counts[Heuristics.PL]/totL, pPR = counts[Heuristics.PR]/totR, pNL = counts[Heuristics.NL]/totL,  pNR = counts[Heuristics.NR]/totR; 
		
		return (totL/tot)*(totR/tot)*
				Math.pow(Math.abs(pPL-pPR)/Math.abs(pPL+pPR)+Math.abs(pNL-pNR)/Math.abs(pNL+pNR),2); 
		
	}

	// P for postive, N for negative, u for unknown memb. examples
	// L/R for left/right branch 
	public static final int PL=0;
	public static final int NL=1;
	public static final int UL=2;
	public static final int PR=3;
	public static final int NR=4;
	public static final int UR=5;

}
