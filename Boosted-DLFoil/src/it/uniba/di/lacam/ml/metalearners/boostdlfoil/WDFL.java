package it.uniba.di.lacam.ml.metalearners.boostdlfoil;

import java.util.ArrayList;
import java.util.HashMap;

import org.semanticweb.owlapi.dlsyntax.renderer.DLSyntaxObjectRenderer;
import org.semanticweb.owlapi.io.OWLObjectRenderer;
import org.semanticweb.owlapi.model.OWLClassExpression;


/**
 * A weak learner that uses a ref.op based on stocastic search. Obtained modifying DL-Foil 
 * @author Giuseppe
 *
 */
public class WDFL {
	static final OWLObjectRenderer renderer = new DLSyntaxObjectRenderer();

	public WDFL() {
		// TODO Auto-generated constructor stub
	}
	
	/**
	 * Generate the specializations and determines the best one
	 * @param prob
	 * @param currConcept, the concept to be refined
	 * @param covPos, the training positive examples
	 * @param covNeg, the training negative examples
	 * @param covUnd, the training unlabeled examples
	 * @param posweights, the weights of the positive examples (decreased in case of correct coverage, increased in case of uncorrect coverage)
	 * @param negweights, the weighes of the negative examples
	 * @param undweights, the weights of the unlabeled
	 * @return the best class expression
	 */
	protected static OWLClassExpression getBestSpecialization2(LProblem prob, OWLClassExpression currConcept, 
			ArrayList<Integer> covPos, ArrayList<Integer> covNeg, ArrayList<Integer> covUnd, HashMap<Integer,Double> posweights, HashMap<Integer,Double> negweights, HashMap<Integer,Double> undweights) {


		OWLClassExpression bestConcept = null;
		double bestGain = - Double.MAX_VALUE;	


		double ocomm_11= WeightsUtils.weights(prob, prob.dataFactory.getOWLObjectComplementOf(currConcept), posweights);
		double ocomm1_1= WeightsUtils.weights(prob, currConcept, negweights);
		//		    // induction
		double oind_10= WeightsUtils.weights(prob, prob.dataFactory.getOWLObjectComplementOf(currConcept), undweights);
		double oind10= WeightsUtils.weights(prob, currConcept, undweights);

		//match rate of former definition
		double omatch11= WeightsUtils.weights(prob, currConcept, posweights);
		double omatch_1_1= WeightsUtils.weights (prob,prob.dataFactory.getOWLObjectComplementOf(currConcept), negweights);
		double omatch00 = WeightsUtils.weights(prob, currConcept,undweights,0);
		double omatch= omatch00+omatch11+omatch_1_1;
		double ocomm=ocomm1_1+ocomm_11;
		double oind=oind10+oind_10;
		double oomissions= WeightsUtils.sum(posweights)+WeightsUtils.sum(negweights)-omatch11-omatch_1_1-ocomm;

		int i=0;
		while (bestGain <= 0) {
			System.out.printf("\nSpecializing %s\n %d\t %d\t %d\n", 
					renderer.render(currConcept), covPos.size(), covNeg.size(), covUnd.size());


			for (int c=0; c<(prob.nCandSubConcepts); ++c) {
				ArrayList<Integer> 	newCovPos=null, newCovNeg=null, newCovUnd=null; 
				ArrayList<Integer> 	newCovPosNeg=null, newCovNegPos=null; // commissions
				ArrayList<Integer>  newCovUndPos=null, newCovUndNeg=null; //inductions
				ArrayList<Integer>  newCovPosUnd=null, newCovNegUnd=null; //inductions

				do {
					OWLClassExpression refConcept = RefinementOperator.rho(RandomGenerator.generator,prob, currConcept);

					// add a lookhead 
					//refConcept = rho(prob, refConcept);


					double thisGain = 0;

					newCovPos = CoverFunction.covered(prob, refConcept, covPos); // match 1 vs 1


					double match11= WeightsUtils.weights(newCovPos, posweights);					
					if (newCovPos.size()>0) {

						newCovNeg = CoverFunction.covered(prob, prob.dataFactory.getOWLObjectComplementOf(refConcept), covNeg);	//match -1 vs -1 	
						newCovUnd = (ArrayList<Integer>)covUnd.clone(); // match 0 vs 0
						//					    ArrayList<Integer> covered = CoverFunction.covered(prob, refConcept, covUnd);
						//						newCovUnd.removeAll(covered);
						//					    newCovUnd.removeAll(CoverFunction.covered(prob, prob.dataFactory.getOWLObjectComplementOf(refConcept), newCovUnd))
						//						

						// commissions
						newCovPosNeg=CoverFunction.covered(prob, prob.dataFactory.getOWLObjectComplementOf(refConcept), covPos);		
						newCovNegPos=CoverFunction.covered(prob, refConcept, covNeg);

						// induzioni
						newCovUndNeg=CoverFunction.covered(prob, prob.dataFactory.getOWLObjectComplementOf(refConcept), covUnd);
						newCovUndPos= CoverFunction.covered(prob, refConcept, covUnd);

						// omissioni: differenza tra covPos-newCovPos-newCovPosNeg
						newCovPosUnd=WeightsUtils.omissions(covPos, newCovPos, newCovPosNeg);
						// omissioni: differenza tra covNeg-newCovNeg-newCovUndNeg
						newCovNegUnd= WeightsUtils.omissions(covNeg, newCovNeg, newCovNegPos);
						// match 0 vs 0: unlabeled- induction rate
						newCovUnd.removeAll(newCovPosUnd);
						newCovUnd.removeAll(newCovNegUnd);

						double match_1_1= WeightsUtils.weights(newCovNeg, negweights);
						double match00= WeightsUtils.weights(newCovNeg, negweights);


						// commission weighted
						double com1_1= WeightsUtils.weights(newCovPosNeg, posweights);
						double com_11= WeightsUtils.weights(newCovNegPos, negweights);

						// omissions weights
						double om0_1= WeightsUtils.weights(newCovNegUnd,negweights);
						double om01= WeightsUtils.weights(newCovPosUnd, posweights);

						// induction weights

						double ind10= WeightsUtils.weights(newCovUndPos, undweights);
						double ind_10=WeightsUtils.weights(newCovUndNeg, undweights);

						// overall classifications
						double match= match11+match00+match_1_1;
						double commission=  com1_1+com_11;
						double omissions= om01+om0_1;
						double inductions= ind10+ind_10;



						thisGain =   Heuristics.corr(omatch, ocomm+oomissions,oind, match,commission+omissions,inductions);

						System.out.printf("%4d. %s\n \t\t %+7e", 
								c, renderer.render(refConcept), thisGain);

						if (thisGain > bestGain) {
							bestConcept = refConcept;
							bestGain = thisGain;
							System.out.println(" <--- current BEST");
						}
						else
							System.out.println();
					}
				} while (newCovPos.size()==0);
			} //
			//if (bestGain <= 0) {
			//	System.out.print("\nNo good specialization found. Trying again... \n");
			//}
		} // while



		System.out.printf("************ best: %e\t %s\n", bestGain, renderer.render(bestConcept));

		return bestConcept;
	}


}
