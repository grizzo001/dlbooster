package it.uniba.di.lacam.ml.metalearners.boostdlfoil;


import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import com.google.common.base.*;

import it.uniba.di.lacam.ml.metalearners.boostdlfoil.CoverFunction;
import it.uniba.di.lacam.ml.metalearners.boostdlfoil.Heuristics;
import it.uniba.di.lacam.ml.metalearners.boostdlfoil.LProblem;
import it.uniba.di.lacam.ml.metalearners.boostdlfoil.RandomGenerator;
import it.uniba.di.lacam.ml.metalearners.boostdlfoil.RefinementOperator;

import org.apache.commons.math3.stat.descriptive.summary.Sum;
import org.semanticweb.owlapi.dlsyntax.renderer.DLSyntaxObjectRenderer;
import org.semanticweb.owlapi.io.OWLObjectRenderer;
import org.semanticweb.owlapi.model.OWLClassExpression;
import org.semanticweb.owlapi.model.OWLObjectAllValuesFrom;
import org.semanticweb.owlapi.model.OWLObjectComplementOf;
import org.semanticweb.owlapi.model.OWLObjectProperty;
import org.semanticweb.owlapi.model.OWLObjectSomeValuesFrom;
import org.semanticweb.owlapi.model.OWLRestriction;
import org.semanticweb.owlapi.model.ClassExpressionType;
import org.semanticweb.owlapi.model.OWLClass;



/**
 * A boosted version of a concept learner  based on DL-Foil(wDFL)  
 * * @author Giuseppe Rizzo
 *
 */
public class BoostWDFL  implements StrongLearner{

	static final OWLObjectRenderer renderer = new DLSyntaxObjectRenderer();
	private static HashMap<Integer, Double> posweights;
	private static HashMap<Integer, Double> negweights;
	private static HashMap<Integer, Double> undweights; 




	/**
	 * The procedure to induce an Ensemble model
	 * @param prob, the learning problem
	 * @param posExs, as a list of integer
	 * @param negExs,as a list of integer
	 * @param undExs,as a list of integer
	 * @return a collection of concept description with their confidence
	 */
	@SuppressWarnings("unchecked")
	public static Collection<Triple<OWLClassExpression,Double, Double>> induceConcept(LProblem prob, ArrayList<Integer> posExs, ArrayList<Integer> negExs, ArrayList<Integer> undExs) {		

		System.out.printf("\n Problem\t p:%d\t n:%d\t u:%d\t\n", 
				posExs.size(), negExs.size(), undExs.size());


		initializeweights(posExs, negExs, undExs);

		// set of disjuncts that make up the final concept
		ArrayList<Triple<OWLClassExpression,Double, Double>> disjuncts = new ArrayList<Triple<OWLClassExpression,Double, Double>>(); 

		OWLClassExpression newConcept = prob.top;
		 

		ArrayList<Integer> covPos = null, covNeg = null, covUnd = null;
		//number of ensemble
		for (int i =0; i<CEClassificationModel2.EnsembleSize; i++) {
			double wpos= WeightsUtils.weights(prob,newConcept,posweights)+ WeightsUtils.weights(prob,prob.dataFactory.getOWLObjectComplementOf(newConcept), negweights)+WeightsUtils.weights(prob, newConcept,undweights,0);

			double initScore= 0;
						

			// Fixed: previous bug confidence on newConcept instead of best concept
			newConcept= WDFL.getBestSpecialization2(prob, newConcept, posExs, negExs, undExs,posweights,negweights,undweights);
			
			System.out.printf("\nWeak Model");
			System.out.printf("%s\n\n", renderer.render(newConcept));


			Double d = WeightsUtils.confidence(prob, newConcept,posweights,negweights,undweights);
			Double d2= WeightsUtils.confidence(prob, prob.dataFactory.getOWLObjectComplementOf(newConcept),posweights,negweights,undweights);
			disjuncts.add(new Triple(newConcept,d,d2));
			

			WeightsUtils.updateProbabilities(prob, posExs, negExs, undExs, newConcept, posweights, negweights, undweights);
			//pos2Cover = CoverFunction.removeCovered(prob, newConcept, pos2Cover);
			newConcept = prob.top;


		}	

		System.out.println("\n FINAL ENSEMBLE:\n\n");
		for (Triple<OWLClassExpression, Double,Double> c: disjuncts)
			System.out.println(renderer.render(c.getLeft())+" ("+c.getRight()+","+c.getRight2()+")");

		return disjuncts;

	}


	private static Double loss(LProblem prob, OWLClassExpression concept) {
		// TODO Auto-generated method stub
		double wpos= WeightsUtils.weights(prob, concept, posweights);
		double wneg= WeightsUtils.weights(prob, concept, negweights);
		Double f= 1- wpos-wneg +wpos*Math.exp(-1*wpos)+wneg*Math.exp(-1*wneg);
		return f;
	}


	private static void initializeweights(ArrayList<Integer> posExs, ArrayList<Integer> negExs, ArrayList<Integer> undExs) {
		posweights= new HashMap();
		negweights= new HashMap();
		undweights= new HashMap();
		System.out.println("Initial weights:"+(double)1/(posExs.size()+negExs.size()+undExs.size()));
		posExs.forEach((f)-> posweights.put(f, (double)1/(posExs.size()+negExs.size()+undExs.size()))); // uniform weights
		negExs.forEach((f)-> negweights.put(f, (double)1/(posExs.size()+negExs.size()+undExs.size())));
		undExs.forEach((f)-> undweights.put(f, (double)1/(posExs.size()+negExs.size()+undExs.size())));

		WeightsUtils.initializeWeights(posweights, negweights, undweights);


	}


	/**
	 * A wrapper method to invoke the coverage function
	 * @param prob
	 * @param concept
	 * @param covExs
	 * @return
	 */
	public static ArrayList<Integer> covered(LProblem prob, OWLClassExpression concept, ArrayList<Integer> covExs) {
		return CoverFunction.covered(prob, concept, covExs);
	}








} // class
