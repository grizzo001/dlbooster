# DL-Booster: README #

An algorithm to induce an ensemble of concepts in  OWL2  using  JFact+OWLAPI 5 using an ADABOOST-like approach.

### What is this repository for? ###

* Inductive classification in  Description Logics (OWL2) knowledge base
* 0.1

### How do I get set up? ###

* Summary of set up
* Configuration
 - Clone the repository via Git
 - Import the project as a Maven project
* Dependencies
 1. main: JFact 5.0.0, OWLAPI 5
 2. other: apache-commons - lang3, math3, configuration2, beanutils
 All the required libraries can be  imported through Maven 

### How to run?###
The  concept learner can be configured through an xml file (e.g. ntn.xml available in the source code). 
The user can specify the configuration of the algorithm and the design of the experiments (cross-validation/bootstrap)
An example of configuration file is reported below:

```xml
<?xml version="1.0" encoding="UTF-8" ?>

<config>

<kb>
<source>file:////C:/Users/Giuseppe/ontos/NTNcombined.owl<source>
<reasoner>jfact</reasoner><!-- currently supported: jfact (default), hermit-->
</kb>

<!-- algorithm section -->
<algo>
	<!-- type of algo -->
	<!-- more algorithms can be run in the experiments (e.g a threewise cmparison among DLFoil, DLFocl1 and TDT learner)-->
	<!-- currently supported: Boosting DL-Foil -->
	<types>
	<type>BDLFoil</type>
	</types>
	<!-- num. of candidate refinements per turn -->
	<NC> 50 </NC>
	<!--number of repetitions for DL-Focl1 and 4-->
	<nEnsemble>3</nEnsemble>
</algo>

<!-- seed for random numbers generators -->
<seed> 1 </seed>

<!-- evaluation section (currently supported: .632 bootstrap, cross validation -->
<evaluation>
	<design>cv</design>
	<!-- rand target concepts -->
 	<NRT> 2 </NRT> 
	<!-- num. of repeatitions/ folds  -->
	<NF> 5 </NF>
	
<!-- target class expressions (Manchester syn) section. -->
	<targets>
<!--  <targets></targets>(Empty): if the concepts must be  randomly generated  -->
	 	<target>
		Agent and (residentPlace some (City or subregion some (not GeographicLocation)))
		</target>
 	 	  <target> 
 		not (Woman) or knows some (not God) 
 		</target>

	</targets>

</evaluation>

</config>
```

##Further material:##
The repository contains (beside the source code) two folders organized as follows:
  * folder “experiments/”:  
		* folder “ontos/”  with the OWL KBs employed in the experiments; 
		* folder “dataset/” containing (for each ontology) both the file with target concepts (and the related distribution of the individuals w.r.t. the labels) and the file with the bootstrap training/test samples
		* an executable jar file through the usual command:
		``` java -jar <jar filename> <xml configuration file>```
  * folder “docs/” with an appendix with technical details on the optimization analysis involving c_t and Z_t in the ternary settings


