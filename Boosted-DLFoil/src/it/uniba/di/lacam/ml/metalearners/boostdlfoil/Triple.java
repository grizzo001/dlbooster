package it.uniba.di.lacam.ml.metalearners.boostdlfoil;

/**
 * An utility class to store information
 * @author Giuseppe
 * @param <L>
 * @param <R>
 * @param <S>
 */
public class Triple<L extends Comparable,R,S> implements Comparable {

	  private final L left;
	  private final R right;
	  private final S right2;

	  public Triple(L left, R right, S right2) {
	    this.left = left;
	    this.right = right;
	    this.right2= right2;
	  }

	  public L getLeft() { return left; }
	  public R getRight() { return right; }
	  public S getRight2() { return right2; }

	  @Override
	  public int hashCode() { return left.hashCode() ^ right.hashCode(); }

	  @Override
	  public boolean equals(Object o) {
	    if (!(o instanceof Triple)) return false;
	    Triple pairo = (Triple) o;
	    return this.left.equals(pairo.getLeft());
	  }
	  
	  public int compareTo(Object o) {
		  
		  if (!(o instanceof Triple)) return -1;
		  Triple pairo = (Triple) o;
		  return this.left.compareTo(pairo.getLeft());
	  }

	}